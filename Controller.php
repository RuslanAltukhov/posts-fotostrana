<?php
/**
 * Created by PhpStorm.
 * User: Taxphone 1
 * Date: 20.12.2017
 * Time: 9:10
 */

class Controller
{
    /**
     * Отображение списка постов
     */
    public function index(){
        $data = Model::getPostsFull();
        $this->render($data);
    }

    /**
     * Создание поста
     */
    public function create()
    {
        if(isset($_POST['content'] ) && !empty($_POST['content']))
        Model::storePost($_POST['content'] ?? '');
        $this->redirect("/?action=index");
    }

    /**
     * Ставим  лайк
     */
    public function like()
    {

        Model::addLike($_GET['post_id'] ?? 0, UID);
        $this->redirect("/?action=index");
    }
    public function unlike()
    {
        Model::deleteLike($_GET['post_id'] ?? 0, UID);

        $this->redirect("/?action=index");
    }
    private function render($data)
    {
        require_once "view.php";
        header("Content-type:text/html;charset=UTF-8");
    }
    private function redirect($url)
    {
        header("Location:{$url}");
    }
}