<?php
/**
 * Created by PhpStorm.
 * User: Taxphone 1
 * Date: 20.12.2017
 * Time: 9:29
 */

class DB
{
    private $DB;
    private static $_instance;

    private function __construct()
    {
        $this->DB = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) or die("Не удается установить соединение с БД");
    }

    public function execute($query, $return_raw = false)
    {

        $result = $this->DB->query($query);
        return is_bool($result) ? $result : call_user_func(function () use ($result, $return_raw) {
            if ($return_raw) return $result;
            $data = [];
            while ($row = $result->fetch_assoc()) $data[] = $row;
            return $data;
        });

    }

    public function beginTransaction()
    {
        $this->DB->begin_transaction();
    }

    public function commit()
    {
        $this->DB->commit();
    }

    public function rollback()
    {
        $this->DB->rollback();
    }

    public function count($table, $where)
    {
        $result = $this->DB->query("SELECT count(*) AS cnt FROM {$table} WHERE " . $where);
        return is_bool($result) ? $result : $result->fetch_assoc()['cnt'];
    }

    public static function getInstance()
    {
        if (self::$_instance)
            return self::$_instance;
        else
            return self::$_instance = new self();
    }

    public static function redirect($url)
    {
        header("Location:{$url}");
    }
}