<?php
ob_start();
session_start();
if(!isset($_SESSION['uid']))
    $_SESSION['uid'] = rand(1,10000000);

/**
 * Created by PhpStorm.
 * User: Taxphone 1
 * Date: 20.12.2017
 * Time: 9:10
 */
define("DB_USER",'root');
define("DB_NAME",'posts');
define("UID", $_SESSION['uid']);
define("DB_PASSWORD",'proger_271992');
define("DB_HOST","localhost");
define("PROJECT_DIR", dirname(__FILE__));

function autoload($name)
{
    require_once PROJECT_DIR . DIRECTORY_SEPARATOR . $name . ".php";
}
spl_autoload_register("autoload");
global $db;
$db = DB::getInstance();
$request_uri = $_SERVER['REQUEST_URI'];
$controller = new Controller();
$action = $_GET['action'] ?? 'index';
if(method_exists($controller,$action))
 $controller->{$action}();
else {
    throw  new \InvalidArgumentException("Такой адрес не существует");
}
ob_flush();
/**
 * @param $name
 */
