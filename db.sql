-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

-- Таблица категорий
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Таблица с лайками. Имеется индекс на колонку post для поиска лайков к определенному посту
DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `post` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post` (`post`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--Таблица с постами. Имеется индекс для поиска постов по категории
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `content` varchar(200) NOT NULL,
  `category` int(20) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2017-12-22 09:34:58