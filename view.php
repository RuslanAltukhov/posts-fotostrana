<html>
 <head>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
 </head>
<body>
 <div class="container" >
     <form method="post" action="/?action=create">
         <div class="form-group">
             <label for="content">Текст поста</label>
             <input type="text" class="form-control" name="content" id="content" placeholder="Текст поста">
         </div>

         <button type="submit" class="btn btn-default">Создать</button>
     </form>
     <?foreach($data['posts'] as $post):?>
         <div class="panel panel-default">
             <div class="panel-body">
                 <div class="row" >
                     <div class="col-md-12" >
                         <?=$post['content'];?>
                     </div>
                 </div>
                <div class="row" >
                    <div class="col-md-6" >
                        <a href="/?action=<?=isset($data['likes'][$post['id'].'_'.UID]) ? "unlike" : "like"?>&post_id=<?=$post['id']?>" class="btn btn-info">
                            <span class="glyphicon glyphicon-heart"></span> <?=isset($data['likes'][$post['id'].'_'.UID]) ? "Не нравится" : "Нравится"?>
                        </a>
                    </div>
                    <div class="col-md-6" >

                    </div>
                </div>

             </div>
         </div>
     <?endforeach;?>

 </div>
</body>
</html>