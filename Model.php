<?php
/**
 * Created by PhpStorm.
 * User: Taxphone 1
 * Date: 20.12.2017
 * Time: 9:10
 */

class Model
{
    public static function getPosts()
    {
        global $db;
        $data = $db->execute("SELECT id, content, category  FROM post");


        return !is_array($data) ? [] : $data;
    }

    public function getPostsFull()
    {
        /**
         * Поскольку вложенный запрос будет работать очень медленно при больших объемах данных, то сначала мы выбираем страницу с постами,
         * Потом выбираем лайки для выбранных постов из другой таблицы. Таким образом данные легко масштабировать та как мы не используем JOIN и вложенные запросы
         */

        $posts = self::getPosts();


        $post_ids = []; //массив c id постов для выборки лайков только по выбранным постам
        array_walk($posts, function (&$item) use (&$post_ids) {
            $post_ids[] = $item['id'];
        });
        $likes =   self::getLikes(' WHERE post IN(' . implode(",", $post_ids) . ')');
        return ['posts' => $posts, 'likes' => $likes];
    }

    public static function getLikes($where = '')
    {
        global $db;
        $result = $db->execute("SELECT post, user_id FROM likes " . $where, true);
        if(is_bool($result)) return [];
        $likes = [];
        while ($row = $result->fetch_assoc())
            $likes[$row['post'] . '_' . $row['user_id']] = $row;
        return $likes;
    }

    public static function deleteLike($post_id, $uid)
    {
        global $db;
        return $db->execute("DELETE FROM likes WHERE post={$post_id} AND user_id={$uid}");
    }

    public static function addLike($post_id, $uid)
    {

        global $db;
        //При вставке данных id формируем путем склеивания $uid и $post_id. Таким образом нам не придется делать проверку при добавление лайка на уникальность этого лайка
        return $db->execute("INSERT likes SET id='{$uid}{$post_id}',post={$post_id}, user_id={$uid}"); //Вставляем новый лайк в таблицу
    }

    public static function storePost($content)
    {
        global $db;

        return $db->execute("INSERT INTO post SET content='" . htmlspecialchars($content) . "'");
 }

}